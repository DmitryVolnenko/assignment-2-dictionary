%define SHIFT 8

extern string_equals
extern string_length

global find_word

section .text


find_word:
	
    .loop:
        add rsi, SHIFT        
        push rdi            ; save
        push rsi            ; save
        call string_equals  ; compare
        pop rsi             ; take
        pop rdi             ; take

	test rax,rax
	jnz .finish
	
	move rsi, [rsi]
	cmp rsi,0
	jne .loop
    .not_good:
	xor rax,rax
	ret

    .finish:
	mov rax, rsi
	ret
	
     	
	
