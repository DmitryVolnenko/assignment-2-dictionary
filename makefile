ASM = nasm
ASMFLAGS=-f elf64
LD=ld


lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

words.o: words.inc
	$(ASM) $(ASMFLAGS) -o words.o words.inc

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o dict.o dict.asm

main.o: main.asm words.o lib.o dict.o
	$(ASM) $(ASMFLAGS) -o main.o main.asm

program: main.o words.o dict.o lib.o
	$(LD) -o program main.o words.o dict.o lib.o
	make clean

.PHONY: clean

clean:
	rm -f *.o


