%define buffer_s 256
%define 0x8 SHIFT
extern find_word
global _start

section .bss
buffer: times buffer_size db 0

section .rodata
%include "lib.inc"
%include "words.inc"

section .data
    overflow_error: db "Overflow", 10, 0
    not_key: db "Key isn't found", 10, 0

section .text

_start:

    mov rdi,buffer ; start buf position
    mov rsi,buffer_s ; buffer size
    call read_word
    cmp rax, 0
    je .size_err
    
    mov rdi,rax
    mov rsi, next
    push rdx
    call find_word
    pop rdx
    test rax,rax
    je .name_err

    mov rdi, rax
    add rdi, SHIFT
    add rdi, 0x1
    add rdi,rdx
    call print_string

    .finish:
	call print_newline
	call exit

    .size_err:
	mov rdi, size_err
	call error
	jmp .finish

    .name_err:
	mov rdi, name_err
	call error
	jmp .finish
