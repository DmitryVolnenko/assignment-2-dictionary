%define n_elem 0

%macro colon 2
    %ifid %2
        %2: dq n_elem
    %else
        %error "second argument must have id form"
    %endif

    %ifstr %1
        db %1, 0
    %else 
        %error "first argument must have string form"
    %endif
    %define n_elem %2
%endmacro
